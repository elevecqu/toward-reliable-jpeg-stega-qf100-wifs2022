import os
import jpegio as jio
import numpy as np
import multiprocessing as mp

from skimage.util import view_as_blocks

from utils import decompress_structure
from embed_juni import embed_img


def img_generator(dir_path, names=None):
    if names is not None:
        for name in names:
            path = os.path.join(dir_path, name)
            tmp = jio.read(path)
            img = decompress_structure(tmp)[:, :, 0].astype(np.float32)
            yield img
    else:
        for name in os.listdir(dir_path):
            path = os.path.join(dir_path, name)
            tmp = jio.read(path)
            img = decompress_structure(tmp)[:, :, 0].astype(np.float32)
            yield img


def get_train_test_generator(dir_path, train_size, stego_percentage):
    """
    Return two generators. One for the train cover images, the second for the test cover images and a third for the
    test stego images
    :param dir_path: images directory path
    :param train_size: percentage of the training set size
    :param stego_percentage: percentage of the stego images among the test images
    :return: three generators.
    """

    # TODO: identify why some images raise a "Premature end of JPEG file" and correct it.
    # TODO: Read .pgm and compress them in .jpeg.

    names = os.listdir(dir_path)
    n = len(names)
    n_train = int(n * train_size)
    n_normal = int((n - n_train) * (1 - stego_percentage))

    return img_generator(dir_path, names[: n_train]), \
           img_generator(dir_path, names[n_train: n_train + n_normal]), \
           (os.path.join(dir_path, name) for name in names[n_train + n_normal:])


def embed_generator(path_generator, output_path, payload):
    """
    Embed random messages into images with J-UNIWARD
    :param path_generator: image path generator
    :param output_path: where to store stego image after embedding
    :param payload: payload of the message in bpnzac
    :return: a generator of stego images
    """
    with mp.Pool() as p:
        try:
            for res in p.imap_unordered(embed_img, ((path, output_path, payload) for path in path_generator)):
                yield res
        except StopIteration as ex:
            stats = ex.value
            return stats


def variance_filter(img_generator, variance_threshold, block_per_threshold):
    """
    Filter images based on the variance of each block. If a block does not have enough variance, it is discarded. The
    whole image is discarded if too many blocks are discarded
    :param img_generator: image generator
    :param variance_threshold: block with a variance below this threshold are discarded
    :param block_per_threshold: image with a percentage of accepted blocks below this threshold are discarded
    :return: a generator of blocks
    """
    ignored = 0
    try:
        for img in img_generator:
            view = view_as_blocks(img, (8, 8))
            mask_var = np.var(view, axis=(2, 3)) >= variance_threshold
            mask_saturated = np.any(view == 255, axis=(2,3)) | np.any(view == 0, axis=(2,3))
            remaining_blocks = view.reshape((-1, 8, 8))[mask_var.flatten() & ~mask_saturated.flatten()]
            if remaining_blocks.shape[0] / np.product(view.shape[:2]) < block_per_threshold:
                ignored += 1
                continue
            else:
                yield remaining_blocks
    except StopIteration as ex:
        stats = ex.value
        stats['Removed by the filter'] = ignored
        return stats


def feature_extractor(block_generator):
    """
    Extract the spatial rounding error from views
    :param block_generator: a generator of list of blocks. One list for one image
    :return: a generator of views
    """
    try:
        for blocks in block_generator:
            yield np.round(blocks) - blocks
    except StopIteration as ex:
        stats = ex.value
        return stats
