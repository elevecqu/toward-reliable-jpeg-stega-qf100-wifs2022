import numpy as np
import matplotlib.pyplot as plt
import multiprocessing as mp

from scipy.stats import kstest

from data import get_train_test_generator, embed_generator, variance_filter, feature_extractor, img_generator
from utils import bonferroni_correction

cover_dir = "/home/labaro/Documents/These/datasets/images/alaska/jpeg/qf100"
stego_dir = "/home/labaro/Documents/These/datasets/images/alaska/jpeg/embedded"
compute_stego = True
train_size = 0.01
payload = 0.1
stego_percentage = 0.1
variance_threshold = 20
block_per_threshold = 0.5
threshold = np.geomspace(1e-4, 1, 100)


def ks_test(error_tuple):
    ref_cdf, error = error_tuple
    return [kstest(ref_cdf[:, pos], error.reshape((-1, 64))[:, pos])[1] for pos in range(64)]


if __name__ == "__main__":
    if compute_stego:
        train_gen, test_cover_gen, stego_names_gen = get_train_test_generator(cover_dir, train_size, stego_percentage)
        stego_gen = embed_generator(stego_names_gen,
                                    stego_dir,
                                    payload)
    else:
        train_gen, test_cover_gen, _ = get_train_test_generator(cover_dir, train_size, 0)
        stego_gen = img_generator(stego_dir)

    test_stego_features = feature_extractor(variance_filter(stego_gen,
                                                            variance_threshold,
                                                            block_per_threshold))
    train_features = feature_extractor(variance_filter(train_gen,
                                                       variance_threshold,
                                                       block_per_threshold))
    test_cover_features = feature_extractor(variance_filter(test_cover_gen,
                                                            variance_threshold,
                                                            block_per_threshold))

    ref_cdf = np.concatenate([error for error in train_features]).reshape((-1, 64))

    y = []
    label = []

    with mp.Pool() as p:
        try:
            for p_val in p.imap_unordered(ks_test, ((ref_cdf, error) for error in test_stego_features)):
                corrected_p = bonferroni_correction(p_val)
                y.append(np.min(corrected_p))
                print(np.min(corrected_p))
                label.append(1)
        except StopIteration as ex:
            stats = ex.value
            print(stats)

        try:
            for p_val in p.imap_unordered(ks_test, ((ref_cdf, error) for error in test_cover_features)):
                corrected_p = bonferroni_correction(p_val)
                y.append(np.min(corrected_p))
                label.append(0)
                print(np.min(corrected_p))
        except StopIteration as ex:
            stats = ex.value
            print(stats)

    y = np.array(y)
    label = np.array(label)

    plt.plot(threshold, [np.mean(y[label == 0] < t) for t in threshold])
